package com.vaadin.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;

public class addCustomerBtnAction implements ApplicationContextAware, ComponentEventListener<ClickEvent<Button>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Grid<Customer> grid;
	private CustomerForm form;
	private ApplicationContext ctx;

	public void setApplicationContext(ApplicationContext applicationContext) {
        this.ctx = applicationContext;
    }
	
	@Override
	public void onComponentEvent(ClickEvent<Button> event) {
		grid.asSingleSelect().clear();
		form.setCustomer((Customer) ctx.getBean("customer"));
	}

	public void setGrid(Grid<Customer> grid) {
		this.grid = grid;
	}

	public void setForm(CustomerForm form) {
		this.form = form;
	}

}