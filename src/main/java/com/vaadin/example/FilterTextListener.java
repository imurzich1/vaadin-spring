package com.vaadin.example;

import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.HasValue.ValueChangeListener;
import com.vaadin.flow.component.textfield.TextField;

public class FilterTextListener implements ValueChangeListener<ComponentValueChangeEvent<TextField, String>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UpdateList updateList;

	
	
	public FilterTextListener(UpdateList updateList) {
		super();
		this.updateList = updateList;
	}

	@Override
	public void valueChanged(ComponentValueChangeEvent<TextField, String> event) {
		updateList.updateList();
	}


}
