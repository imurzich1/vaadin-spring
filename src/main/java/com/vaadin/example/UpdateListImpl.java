package com.vaadin.example;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.textfield.TextField;

public class UpdateListImpl implements UpdateList {

	private Grid<Customer> grid;
	private CustomerService service;
	private TextField filterText;
	
	public UpdateListImpl(Grid<Customer> grid, CustomerService service) {
		super();
		this.grid = grid;
		this.service = service;
	}

	@Override
	public void updateList() {
		grid.setItems(service.findAll(filterText.getValue()));
	}

	public void setFilterText(TextField filterText) {
		this.filterText = filterText;
	}
	
}
