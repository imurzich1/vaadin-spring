package com.vaadin.example;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

/**
 * The main view contains a button and a click listener.
 */
@Route("")
@PWA(name = "Project Base for Vaadin Flow", shortName = "Project Base")
public class MainView extends VerticalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Grid<Customer> grid;
	private CustomerForm form;
	private UpdateList updateList;

	@SuppressWarnings("unchecked")
	public MainView() {
		ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
		grid = (Grid<Customer>)ctx.getBean("grid");
		form = (CustomerForm)ctx.getBean("form");
		updateList = (UpdateList)ctx.getBean("updateList");
		HorizontalLayout toolbar = (HorizontalLayout)ctx.getBean("toolbar");
		HorizontalLayout mainContent = (HorizontalLayout)ctx.getBean("mainContent");
		mainContent.setSizeFull();
		grid.setSizeFull();
		add(toolbar, mainContent);
		setSizeFull();
		updateList.updateList();
		grid.asSingleSelect().addValueChangeListener(event -> form.setCustomer(grid.asSingleSelect().getValue()));
	}
}
